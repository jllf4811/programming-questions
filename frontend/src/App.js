import "./App.css";
import { ProveedorToken } from "./components/ProveedorToken";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PaginaLogin from "./pages/PaginaLogin";
import PaginaRegistro from "./pages/PaginaRegistro";
import EditarPefil from "./components/EditarPerfil";
import PaginaUsuario from "./pages/PaginaUsuario";
import Inicio from "./pages/Inicio";
import PreguntaMasRespuestas from "./pages/PreguntaMasRespuestas";
import CrearPublicacion from "./pages/CrearPublicacion";
import ListarPreguntas from "./componentes/ListarPreguntas";
import ListarBusqueda from "./componentes/ListarBusqueda";
import TusPublicaciones from "./componentes/TusPublicaciones";
import EditarPublicacionRespuestas from "./pages/EditarPublicacionRespuestas";
import EditarPublicacionPreguntas from "./pages/EditarPublicacionPreguntas";
import ActivarUsuario from "./componentes/ActivarUsuario";
import RecuperarContraseña from "./pages/RecuperarContraseña";

function App() {
  return (
    <div className="App">
      <Router>
        <ProveedorToken>
          <Switch>
            <Route exact path="/login">
              <Inicio>
                <PaginaLogin />
              </Inicio>
            </Route>
            <Route exact path="/registro">
              <Inicio>
                <PaginaRegistro />
              </Inicio>
            </Route>
            <Route exact path="/recuperarContraseña">
              <Inicio>
                <RecuperarContraseña></RecuperarContraseña>
              </Inicio>
            </Route>
            <Route exact path="/perfil">
              <Inicio>
                <PaginaUsuario />
              </Inicio>
            </Route>
            <Route exact path="/Perfil/editar">
              <Inicio>
                <EditarPefil />
              </Inicio>
            </Route>
            <Route exact path="/">
              <Inicio>
                <ListarPreguntas />
              </Inicio>
            </Route>
            <Route exact path="/publicarPregunta">
              <Inicio>
                <CrearPublicacion />
              </Inicio>
            </Route>
            <Route exact path="/perfil/publicaciones">
              <Inicio>
                <TusPublicaciones></TusPublicaciones>
              </Inicio>
            </Route>
            <Route exact path="/perfil/publicaciones/respuestas/:id">
              <Inicio>
                <EditarPublicacionRespuestas></EditarPublicacionRespuestas>
              </Inicio>
            </Route>
            <Route exact path="/perfil/publicaciones/preguntas/:id">
              <Inicio>
                <EditarPublicacionPreguntas></EditarPublicacionPreguntas>
              </Inicio>
            </Route>
            <Route exact path="/pregunta">
              <Inicio>
                <ListarBusqueda />
              </Inicio>
            </Route>
            <Route exact path="/pregunta/:id">
              <Inicio>
                <PreguntaMasRespuestas />
              </Inicio>
            </Route>
            <Route exact path="/activar/:codigoRegistro">
              <ActivarUsuario></ActivarUsuario>
            </Route>
          </Switch>
        </ProveedorToken>
      </Router>
    </div>
  );
}

export default App;
