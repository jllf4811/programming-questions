import { useState, useContext, useEffect } from "react";

import { ContextoToken } from "./ProveedorToken";
import { Button,Form } from "react-bootstrap";

//import "./FormularioRegistro.css"
const EditarContrasena = (props) => {
  const [token] = useContext(ContextoToken);
  const [editar, setEditar] = useState(false);
  const [contrasena, setContrasena] = useState("");
  const [contrasenaNueva, setContrasenaNueva] = useState("");
  const [repetirContrasenaNueva, setRepetirContraseñaNueva] = useState("");

  const actualizar = async () => {
    if (contrasenaNueva === repetirContrasenaNueva) {
      const res = await fetch(
        `http://localhost:3050/usuario/contrasena/${localStorage.getItem(
          "id"
        )}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            autorizado: token,
          },
          body: JSON.stringify({
            contrasenaVieja: contrasena,
            nuevaContrasena: contrasenaNueva,
            nuevaContrasena2: repetirContrasenaNueva,
          }),
        }
      );

      if (res.ok) {
        localStorage.clear();
        window.location.reload();
      }
    }
  };

  return (
    <>
      {/* <p>Editar contrasena</p> */}
      {editar ? (
        <>
          <Form>
            <Form.Label>Editar Contraseña</Form.Label>
            <Form.Group>
              <Form.Label>Contraseña actual</Form.Label>
              <Form.Control
                type="password"
                name="contrasena"
                id="contrasena"
                value={contrasena}
                onChange={(e) => {
                  setContrasena(e.target.value);
                }}
              ></Form.Control>
            </Form.Group>
            <Form.Group>
              <Form.Label>Introduce la nueva contraseña</Form.Label>
              <Form.Control
                type="password"
                name="contrasenaNueva"
                id="contrasenaNueva"
                value={contrasenaNueva}
                onChange={(e) => {
                  setContrasenaNueva(e.target.value);
                }}
              ></Form.Control>
            </Form.Group>
            <Form.Group>
              <Form.Label>Repetir nueva contraseña</Form.Label>
              <Form.Control
                type="password"
                name="repetirContrasenaNueva"
                id="repetirContrasenaNueva"
                value={repetirContrasenaNueva}
                onChange={(e) => {
                  setRepetirContraseñaNueva(e.target.value);
                }}
              ></Form.Control>
            </Form.Group>
          </Form>
          {/* <fieldset>
            <label htmlFor="contrasena">contraseña actual</label>
            <input
              type="password"
              name="contrasena"
              id="contrasena"
              value={contrasena}
              onChange={(e) => {
                setContrasena(e.target.value);
              }}
            ></input>
          </fieldset> */}
          {/* <fieldset>
            <label htmlFor="contrasenaNueva">Introduce nueva contraseña</label>
            <input
              type="password"
              name="contrasenaNueva"
              id="contrasenaNueva"
              value={contrasenaNueva}
              onChange={(e) => {
                setContrasenaNueva(e.target.value);
              }}
            ></input>
          </fieldset> */}
          {/* <fieldset>
            <label htmlFor="repetirContrasenaNueva">
              repetir nueva contraseña
            </label>
            <input
              type="password"
              name="repetirContrasenaNueva"
              id="repetirContrasenaNueva"
              value={repetirContrasenaNueva}
              onChange={(e) => {
                setRepetirContraseñaNueva(e.target.value);
              }}
            ></input>
          </fieldset> */}

          <Button
          variant="dark"
            type="button"
            onClick={(e) => {
              actualizar();
              setEditar(false);
            }}
          >
            Aceptar
          </Button>
          <Button
          variant="dark"
            type="button"
            onClick={(e) => {
              setEditar(false);
            }}
          >
            Cancelar
          </Button>
        </>
      ) : (
        <Button
        variant="dark"
          type="button"
          onClick={(e) => {
            setEditar(true);
          }}
        >
          Editar contraseña
        </Button>
      )}
    </>
  );
};

export default EditarContrasena;
