import { useState, useContext, useEffect } from "react";
import { ContextoToken } from "./ProveedorToken";
import { Button, Form } from "react-bootstrap";

//import "./FormularioRegistro.css"
const EditarNombre = (props) => {
  const { nombre } = props.children;
  const [token] = useContext(ContextoToken);
  const [editarNombre, setEditarNombre] = useState(false);
  const [nombreCambiar, setNombreCambiar] = useState(nombre);

  const actualizar = async () => {
    if (nombreCambiar) {
      const res = await fetch(
        `http://localhost:3050/usuario/editar/${localStorage.getItem("id")}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            autorizado: token,
          },
          body: JSON.stringify({ nombre: nombreCambiar }),
        }
      );
      if (res.ok) {
        localStorage.setItem("nombre", nombreCambiar);
      }
    } else {
      alert("El parametro no puede estar vacio");
    }
  };

  useEffect(() => {}, [nombreCambiar, setEditarNombre]);

  return (
    <>
      {/* <p>Nombre Editado</p> */}
      {editarNombre ? (
        <>
          <label htmlFor="nombre"></label>
          <Form>
            <Form.Group>
              <Form.Label>Nombre Editado</Form.Label>
              <Form.Control
                type="text"
                name="nombre"
                id="nombre"
                value={nombreCambiar}
                defaultValue={nombre}
                onChange={(e) => {
                  setNombreCambiar(e.target.value);
                }}
              ></Form.Control>
            </Form.Group>
          </Form>
          {/* <input
            type="text"
            name="nombre"
            id="nombre"
            value={nombreCambiar}
            defaultValue={nombre}
            onChange={(e) => {
              setNombreCambiar(e.target.value);
            }}
          ></input> */}
          <Button
            variant="dark"
            type="button"
            onClick={(e) => {
              actualizar();
              setEditarNombre(false);
            }}
          >
            Aceptar
          </Button>
          <Button
            variant="dark"
            type="button"
            onClick={(e) => {
              setEditarNombre(false);
            }}
          >
            Cancelar
          </Button>
        </>
      ) : (
        <Button
          variant="dark"
          type="button"
          onClick={(e) => {
            setEditarNombre(true);
          }}
        >
          Editar nombre
        </Button>
      )}
    </>
  );
};

export default EditarNombre;
