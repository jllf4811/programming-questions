import { Container } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import EditarNombre from "./EditarNombre";
import EditarEmail from "./EditarEmail";
import EditarContrasena from "./EditarContrasena";
import Form from "react-bootstrap/Form";

//import "./FormularioRegistro.css"
import "../style/EstilosComponente/usuario.css";
import { Button } from "react-bootstrap";

const Usuario = (props) => {
  const { nombre, email, rol } = props.children;

  return (
    <div className="Usuario">
      <Form id="usuario">
        <Form.Group size="lg">
          <Form.Label>Nombre</Form.Label>
          <Form.Control
            type="text"
            name="nombre"
            value={nombre}
            readOnly
          ></Form.Control>
          <br></br>
          <EditarNombre>{nombre}</EditarNombre>
        </Form.Group>
        <Form.Group size="lg">
          <Form.Label>Rol</Form.Label>
          <Form.Control
            type="text"
            name="rol"
            value={rol}
            readOnly
          ></Form.Control>
        </Form.Group>

        <Form.Group size="lg">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="text"
            name="Email"
            value={email}
            readOnly
          ></Form.Control>
           <br></br>
          <EditarEmail>{email}</EditarEmail>
        </Form.Group>
        
        <EditarContrasena></EditarContrasena>
      </Form>
    </div>

    // <Container>
    //   <Row>
    //     <Col sm={12}>Datos del Usuario </Col>
    //   </Row>
    //   <Row>
    //     <Col sm>
    //       <EditarNombre>{nombre}</EditarNombre>
    //     </Col>
    //   </Row>
    //   <Row>
    //     <Col sm>{nombre}</Col>
    //   </Row>
    //   <Row>
    //     <Col sm>
    //       <EditarEmail>{email}</EditarEmail>
    //       <Col sm>{email}</Col>
    //     </Col>
    //   </Row>
    //   <Row>
    //     <Col sm>Rol</Col>
    //     <Col sm>{rol}</Col>
    //   </Row>
    //   <Row>
    //     <Col sm>
    //       <EditarContrasena></EditarContrasena>
    //     </Col>
    //   </Row>
    // </Container>
  );
};

export default Usuario;
