import { useState, useContext, useEffect } from "react";
import { ContextoToken } from "./ProveedorToken";
import { Button, Form } from "react-bootstrap";

//import "./FormularioRegistro.css"
const EditarEmail = (props) => {
  const { email } = props.children;
  const [token] = useContext(ContextoToken);
  const [editarEmail, setEditarEmail] = useState(false);
  const [emailCambiar, setEmailCambiar] = useState("");
  const [emailRepetido, setEmailRepetido] = useState("");
  const [contrasena, setContrasena] = useState("");
  const [contrasenaRepetido, setContrasenaRepetido] = useState("");

  const actualizar = async () => {
    if (
      emailCambiar === emailRepetido &&
      contrasena === contrasenaRepetido &&
      emailCambiar &&
      contrasena
    ) {
      const res = await fetch(
        `http://localhost:3050/usuario/editar/${localStorage.getItem("id")}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            autorizado: token,
          },
          body: JSON.stringify({ email: emailCambiar, contrasena }),
        }
      );

      if (res.ok) {
        localStorage.clear();
        window.location.reload();
      }
    } else {
      alert("No coincide el email o la contraseña");
    }
  };

  return (
    <>
      {/* <p>Email</p> */}
      {editarEmail ? (
        <>
          <Form>
            <Form.Label>Editar Email</Form.Label>
            <Form.Group>
              <Form.Label>Introduce email nuevo</Form.Label>
              <Form.Control
                type="email"
                name="email"
                id="email"
                value={email}
                defaultValue={email}
                onChange={(e) => {
                  setEmailCambiar(e.target.value);
                }}
              ></Form.Control>
            </Form.Group>
            <Form.Group>
              <Form.Label>Repite el nuevo email</Form.Label>
              <Form.Control
                type="email"
                name="repetirEmail"
                id="repetirEmail"
                value={emailRepetido}
                defaultValue={email}
                onChange={(e) => {
                  setEmailRepetido(e.target.value);
                }}
              ></Form.Control>
            </Form.Group>
            <Form.Group>
              <Form.Label>Introduce contraseña</Form.Label>
              <Form.Control
                type="password"
                name="password"
                id="password"
                value={contrasena}
                onChange={(e) => {
                  setContrasena(e.target.value);
                }}
              ></Form.Control>
            </Form.Group>
            <Form.Group>
              <Form.Label>Repite contraseña</Form.Label>
              <Form.Control
                type="password"
                name="repetirPassword"
                id="repetirPassword"
                value={contrasenaRepetido}
                onChange={(e) => {
                  setContrasenaRepetido(e.target.value);
                }}
              ></Form.Control>
            </Form.Group>
          </Form>
          {/* <fieldset>
            <label htmlFor="email">Introduce email nuevo</label>
            <input
              type="email"
              name="email"
              id="email"
              value={email}
              defaultValue={email}
              onChange={(e) => {
                setEmailCambiar(e.target.value);
              }}
            ></input>
          </fieldset> */}

          {/* <fieldset>
            <label htmlFor="repetirEmail">repite email nuevo</label>
            <input
              type="email"
              name="repetirEmail"
              id="repetirEmail"
              value={emailRepetido}
              defaultValue={email}
              onChange={(e) => {
                setEmailRepetido(e.target.value);
              }}
            ></input>
          </fieldset> */}

          {/* <fieldset>
            <label htmlFor="password">Introduce contraseña</label>
            <input
              type="password"
              name="password"
              id="password"
              value={contrasena}
              onChange={(e) => {
                setContrasena(e.target.value);
              }}
            ></input>
          </fieldset> */}
          {/* <fieldset>
            <label htmlFor="repetirPassword">Repite contraseña</label>
            <input
              type="password"
              name="repetirPassword"
              id="repetirPassword"
              value={contrasenaRepetido}
              onChange={(e) => {
                setContrasenaRepetido(e.target.value);
              }}
            ></input>
          </fieldset> */}

          <Button
            variant="dark"
            type="button"
            onClick={(e) => {
              actualizar();
              setEditarEmail(false);
            }}
          >
            Aceptar
          </Button>
          <Button
            variant="dark"
            type="button"
            onClick={(e) => {
              setEditarEmail(false);
            }}
          >
            Cancelar
          </Button>
        </>
      ) : (
        <Button
          variant="dark"
          type="button"
          onClick={(e) => {
            setEditarEmail(true);
          }}
        >
          Editar email
        </Button>
      )}
    </>
  );
};

export default EditarEmail;
