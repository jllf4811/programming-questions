import { Link } from "react-router-dom";
import "../style/EstilosComponente/pregunta.css";
import { ListGroup } from "react-bootstrap";

const Pregunta = (props) => {
  const { id, titulo, etiqueta, nombre } = props.children;

  const arrayEtiquetas = etiqueta?.split(",").map((tags, index) => (
    <li key={index} className="etiqueta">
      <Link to={`/pregunta/?buscar=&etiqueta=${tags}&sinRespuestas=`}>{tags}</Link>
    </li>
  ));

  return (
<ListGroup className="cubo_pregunta">
  <ListGroup.Item>
  <Link className="Link" to={`/pregunta/${id}`}>
              <h2 className="preguntaID">{titulo}</h2>
            </Link>
  </ListGroup.Item>
  <ListGroup.Item>{arrayEtiquetas}</ListGroup.Item>
  <ListGroup.Item>{`Formulada por: ${nombre}`}</ListGroup.Item>
</ListGroup>


    // <li>
    //   <section className="cubo_pregunta">
    //     <section className="">
    //       <article>
    //         <Link className="Link" to={`/pregunta/${id}`}>
    //           <h2 className="preguntaID">{titulo}</h2>
    //         </Link>
    //       </article>
    //     </section>
    //     <section></section>
    //     <section>
    //       <nav>
    //         <ol>{arrayEtiquetas}</ol>
    //       </nav>
    //     </section>
    //     <section>
    //       <p>{`Formulada por: ${nombre}`}</p>
    //     </section>
    //   </section>
    // </li>
  );
};

export default Pregunta;
