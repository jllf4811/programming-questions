import Buscador from "./Buscador";
import { Link } from "react-router-dom";
import { useContext } from "react";
import { ContextoToken } from "../components/ProveedorToken";
import { Navbar } from "react-bootstrap";
import { Nav } from "react-bootstrap";
import { NavDropdown } from "react-bootstrap";
import { Button } from "react-bootstrap";
import "../style/EstilosComponente/cabecera.css";

const Cabecera = () => {
  const [token] = useContext(ContextoToken);

  return (
    <>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="/">Programming Questions</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/">Inicio</Nav.Link>
            {localStorage.getItem("rol") === "estudiante" ? (
              <>
                {/* <Nav.Link href="/publicarPregunta">Formular pregunta</Nav.Link> */}
                <Nav.Link>
                  <Link to="/publicarPregunta" className="linkFormularPregunta">
                    Formular pregunta
                  </Link>
                </Nav.Link>
              </>
            ) : (
              ""
            )}
            {token ? (
              <Nav.Link>
                <Link to="/perfil/publicaciones" className="linkTusPublicaciones">
                  Tus Publicaciones
                </Link>
              </Nav.Link>
            ) : (
              <></>
            )}
          </Nav>
          <Buscador></Buscador>

          <section>
            {!token ? (
              <>
                <section className="contenedor_link">
                  <Button variant="dark" className="link">
                    <Link to="/login" className="linkBlanco">
                      Iniciar Sesión
                    </Link>
                  </Button>

                  <Button variant="dark" className="link">
                    <Link to="/registro" className="linkBlanco">
                      Registrarse
                    </Link>
                  </Button>
                </section>
              </>
            ) : (
              <>
                <section>
                  <p>{`Bienvenido, ${localStorage.getItem("nombre")}`}</p>
                </section>
                <section className="contenedor_link">
                  <Button
                    variant="dark"
                    className="contenedorLinkCerrar"
                    href="/"
                    onClick={(e) => {
                      localStorage.clear();
                      window.location.reload();
                    }}
                  >
                    Cerrar Sesión
                  </Button>
                  <Button variant="dark" className="contenedorLinkPerfil">
                    <Link to="/Perfil" className="linkBlanco">
                      Perfil
                    </Link>
                  </Button>
                </section>
              </>
            )}
          </section>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default Cabecera;
