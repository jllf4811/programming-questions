const useEliminarRespuestas = async (id, token) => {
  const res = await fetch(`http://localhost:3050/pregunta/respuesta/${id}`, {
    method: "DELETE",
    headers: {
      autorizado: token,
    },
  });
  const json = await res.json();
  if (res.ok) {
    alert("se ha eliminado corectamente la publicacion");
  } else {
    alert(json.error);
  }
};

export default useEliminarRespuestas;
