const realizarVoto = async (id, valor, token) => {
  await fetch(`http://localhost:3050/respuesta/${id}/voto`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      autorizado: token,
    },
    body: JSON.stringify({ voto: valor }),
  });
};

export default realizarVoto;
