import React from "react";
import voto from "../imagen/triangulo_voto_positivo.svg";
import realizarVoto from "./realizarVoto";
import "../style/EstilosComponente/respuesta.css";
import { useContext } from "react";
import { ContextoToken } from "../components/ProveedorToken";
const Respuestas = (props) => {
  const [token] = useContext(ContextoToken);
  const {
    id,
    nombre,
    votos_positivos,
    votos_negativos,
    fechaActualizacionRespuestas,
    textoRespuesta,
    fechaPublicacionRespuestas,
  } = props.children;
  let fechaActualizacion = new Date(fechaActualizacionRespuestas);
  let fechaCreacion = new Date(fechaPublicacionRespuestas);
  let cantidad = Number(votos_positivos) + Number(votos_negativos);
  return (
    <li className="estilo_respuesta">
      <div className="contenido_respuestas">
        <section className="contenido_voto">
          <div>
            <img
              indexvoto="1"
              draggable="false"
              onClick={(e) => {
                realizarVoto(id, e.target.attributes.indexvoto.value, token);
              }}
              className="voto_positivo"
              src={voto}
              alt="voto_positivo"
              title="voto_negativo"
            />
          </div>
          <div>
            <p>{cantidad}</p>
          </div>
          <div>
            <img
              indexvoto="-1"
              draggable="false"
              onClick={(e) => {
                realizarVoto(id, e.target.attributes.indexvoto.value, token);
              }}
              src={voto}
              className="voto_negativo"
              alt="voto_negativo"
              title="voto_negativo"
            />
          </div>
        </section>
        <section className="textoContenido">
          <article>
            <p>{textoRespuesta}</p>
          </article>
        </section>
        <section className="infoRespuesta">
          <div>
            <p>{`Respondida por: ${nombre}`} </p>
          </div>
          <div>
            <time>{`Creada: ${fechaCreacion.toLocaleString()}`}</time>
          </div>
          <div>
            <time>{`Ultima actulizacion: ${fechaActualizacion.toLocaleString()}`}</time>
          </div>
        </section>
      </div>
    </li>
  );
};

export default Respuestas;
