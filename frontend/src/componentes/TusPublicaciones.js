import usePublicaciones from "../hooks/usePublicaciones";
import { Link } from "react-router-dom";
import eliminarRespuestas from "./eliminarRespuestas";
import eliminarPregunta from "./eliminarPregunta";
import { useContext } from "react";
import { ContextoToken } from "../components/ProveedorToken";
import { Button, Form, ListGroup } from "react-bootstrap";
import "../style/EstilosComponente/tusPublicaciones.css";

const TusPublicaciones = () => {
  const [token] = useContext(ContextoToken);
  const [publicaciones] = usePublicaciones();

  const arrayPreguntas = publicaciones?.map((contenido, index) => {
    if (contenido.rol === "experto") {
      return (
        <Form>
          <ListGroup className="respuestas-publicadas" key={contenido.id}>
            <ListGroup.Item>
              <div>
                <h2>Tu respuesta sobre:</h2> {contenido.titulo}
              </div>
              <br></br>
              <div>
                <h5>Tu respuesta:</h5>
                {contenido.textoRespuesta}
              </div>
            </ListGroup.Item>
          </ListGroup>
          <div className="editarEliminar">
            <Form.Group>
              <Button variant="dark">
                <Link
                  to={`/perfil/publicaciones/respuestas/${index}`}
                  className="enlace"
                >
                  Editar
                </Link>
              </Button>
            </Form.Group>

            <Form.Group>
              <Button variant="dark" className="botones">
                <Link
                  className="enlace"
                  to={`/perfil/publicaciones`}
                  indexEliminar={contenido.id}
                  onClick={(e) => {
                    window.confirm(
                      "seguro que quieres eliminar esta respuesta"
                    ) &&
                      eliminarRespuestas(
                        e.target.attributes.indexEliminar.value,
                        token
                      );
                  }}
                >
                  Eliminar
                </Link>
              </Button>
            </Form.Group>
          </div>
        </Form>

        // <li className="respuestas-publicadas" key={contenido.id}>
        //   <section>
        //     <article>
        //       <h3>Tu respuesta sobre: {contenido.titulo} </h3>
        //       <p>{contenido.textoRespuesta}</p>
        //     </article>
        //   </section>
        //   <section>
        // <div>
        //   <Link to={`/perfil/publicaciones/respuestas/${index}`}>
        //     Editar
        //   </Link>
        // </div>
        //     <div>
        //       <Link
        //         to={`/perfil/publicaciones`}
        //         indexEliminar={contenido.id}
        //         onClick={(e) => {
        //           window.confirm(
        //             "seguro que quieres eliminar esta respuesta"
        //           ) &&
        //             eliminarRespuestas(
        //               e.target.attributes.indexEliminar.value,
        //               token
        //             );
        //         }}
        //       >
        //         Eliminar
        //       </Link>
        //     </div>
        //   </section>
        // </li>
      );
    }
    if (contenido.rol === "estudiante") {
      return (
        <Form>
          <ListGroup className="pregunta-publicadas" key={index}>
            <ListGroup.Item>
              <div>
                <h2>Tu respuesta sobre:</h2> {contenido.titulo}
              </div>
              <br></br>
              <div>
                <h5>Tu pregunta:</h5>
                {contenido.textoPregunta}
              </div>
            </ListGroup.Item>
          </ListGroup>
          <div className="editarEliminar">
            <Form.Group>
              <Button variant="dark">
                <Link
                  to={`/perfil/publicaciones/preguntas/${index}`}
                  className="enlace"
                >
                  Editar
                </Link>
              </Button>
            </Form.Group>
            <Form.Group>
              <Button variant="dark" className="botones">
                <Link
                  className="enlace"
                  to={`/perfil/publicaciones`}
                  indexEliminar={contenido.id}
                  onClick={(e) => {
                    window.confirm(
                      "seguro que quieres elimnar esta respuesta"
                    ) &&
                      eliminarPregunta(
                        e.target.attributes.indexEliminar.value,
                        token
                      );
                  }}
                >
                  Eliminar
                </Link>
              </Button>
            </Form.Group>
          </div>
        </Form>

        // <li className="pregunta-publicadas" key={index}>
        //   <section>
        //     <article>
        //       <h3>{contenido.titulo} </h3>
        //       <p>{contenido.textoPregunta}</p>
        //     </article>
        //   </section>
        //   <section>
        //     <Link to={`/perfil/publicaciones/preguntas/${index}`}>Editar</Link>
        //     <Link
        //       to={`/perfil/publicaciones`}
        //       indexEliminar={contenido.id}
        //       onClick={(e) => {
        //         window.confirm("seguro que quieres elimnar esta respuesta") &&
        //           eliminarPregunta(
        //             e.target.attributes.indexEliminar.value,
        //             token
        //           );
        //       }}
        //     >
        //       Eliminar
        //     </Link>
        //   </section>
        // </li>
      );
    }
  });

  return (
    <div id="contenido">
      {arrayPreguntas.length < 1 ? (
        <>
          <p>No tienes publicaciones</p>
        </>
      ) : (
        <section>
          <nav>
            <ul>{arrayPreguntas}</ul>
          </nav>
        </section>
      )}
    </div>
  );
};

export default TusPublicaciones;
