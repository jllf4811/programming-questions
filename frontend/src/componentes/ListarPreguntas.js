import useCargarPreguntas from "../hooks/useCargarPreguntas";
import Pregunta from "./PreguntasInicio";

const ListarPreguntas = (props) => {
  const [preguntas] = useCargarPreguntas();

  const arrayPreguntas = preguntas?.map((contenido) => (
    <Pregunta id="pregunta" className="pregunta" key={contenido.id}>
      {contenido}
    </Pregunta>
  ));

  return (
    <div>
      <section>
        <nav>
          <ul>{arrayPreguntas}</ul>
        </nav>
      </section>
    </div>
  );
};

export default ListarPreguntas;
