import useBuscador from "../hooks/useBuscador";
import PreguntaInicio from "./PreguntasInicio";

const ListarBusqueda = () => {
  const [busqueda] = useBuscador();

  const arrayPreguntas = busqueda?.map((contenido) => (
    <PreguntaInicio id="pregunta" className="pregunta" key={contenido.id}>
      {contenido}
    </PreguntaInicio>
  ));

  return (
    <div id="contenido">
      <section>
        <nav>
          <ul>{arrayPreguntas}</ul>
        </nav>
      </section>
    </div>
  );
};

export default ListarBusqueda;
