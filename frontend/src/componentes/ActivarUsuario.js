import { useEffect } from "react";
import { useParams, useHistory } from "react-router";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";
import { Button } from "react-bootstrap";

const ActivarUsuario = () => {
  const { codigoRegistro } = useParams();
  const history = useHistory();

  const activar = async () => {
    const res = await fetch(`http://localhost:3050/activar/${codigoRegistro}`, {
      method: "PUT",
      headers: {
        "Conten-type": "application/json",
      },
    });
    const json = await res.json();
  };

  useEffect(() => {
    activar();
  }, []);

  // const redireccionar = () => {
  //   history.push("/");
  // };
  return (
    <>
      <Card>
        <Card.Header>Se ha activado el usuario</Card.Header>
        <Card.Body>
          <Card.Title>Gracias por registrarte</Card.Title>
          <Card.Text>
            Presiona el boton para ir a la pagina de inicio.
          </Card.Text>
          <Button
            variant="primary"
            href="/"
            // onClick={(e) => {
            //   redireccionar();
            // }}
          >
            Ir Inicio
          </Button>
        </Card.Body>
      </Card>
     
    </>
  );
};

export default ActivarUsuario;
