import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

const useBuscador = () => {
  const [busqueda, setBusqueda] = useState([]);

  const queryString = useLocation().search;

  const params = new URLSearchParams(queryString);

  const buscar = params.get("buscar");
  const filtro = params.get("etiqueta");
  const sinRespuestas = params.get("sinRespuestas");

  useEffect(() => {
    const CargarPreguntas = async () => {
      const res = await fetch(
        `http://localhost:3050/pregunta?buscar=${buscar}&filtro=${filtro}&sinRespuestas=${sinRespuestas}`
      );
      const json = await res.json();

      setBusqueda(json);
    };

    CargarPreguntas();
  }, [buscar, setBusqueda]);

  return [busqueda];
};

export default useBuscador;
