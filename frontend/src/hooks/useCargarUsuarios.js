import { useEffect, useState } from "react";

const useCargarUsuarios = (id) => {
  const [usuarios, setUsuarios] = useState([]);

  useEffect(() => {
    const CargarUsuarios = async () => {
      const res = await fetch(`http://localhost:3050/usuario/${localStorage.getItem("id")}`, {
        method: "GET",
        headers: {
          "Content-type": "application/json",
        },
      });

      const json = await res.json();

      setUsuarios(json);
    };
    CargarUsuarios();
    let intervalo = setInterval(CargarUsuarios, 500);

    return () => {
      setUsuarios([]);
      clearInterval(intervalo);
    };
  }, [id]);

  return [usuarios, setUsuarios];
};

export default useCargarUsuarios;
