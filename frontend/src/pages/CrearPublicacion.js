import { useState } from "react";
import { useHistory } from "react-router";
import { useContext } from "react";
import { ContextoToken } from "../components/ProveedorToken";
import Cabecera from "../componentes/Cabecera";
import Footer from "../pages/Footer";
import { Form } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Card } from "react-bootstrap";
import { ListGroup } from "react-bootstrap";

const CrearPublicacion = () => {
  const [token] = useContext(ContextoToken);
  const [inputTitulo, setInputTitulo] = useState("");
  const [textarea_pregunta, setTextarea_Pregunta] = useState("");
  const [inputEtiqueta, setInputEtiqueta] = useState("");
  const history = useHistory();

  const enviarPublicacion = async () => {
    const res = await fetch("http://localhost:3050/pregunta", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        autorizado: token,
      },
      body: JSON.stringify({
        titulo: inputTitulo,
        texto: textarea_pregunta,
        tecnologiaLenguajes: inputEtiqueta,
        imagen: "",
      }),
    });
    const json = await res.json();
    if (res.ok) {
      alert("se ha creado la publicacion");
      redireccionar();
    } else {
      alert(json.error);
    }
  };

  const redireccionar = () => {
    history.push("/");
  };
  return (
    <>
      <div class="principal">
        <div class="divTitulo">
          <div class="titulo">Publica una pregunta</div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <Form className="forumulario/pregunta" method="POST">
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="tituloPregunta">
                    <h1>Titulo</h1>
                    <Form.Label>
                      Al realizar la pregunta Ten encuenta formular la pregunta de una forma clara y
                      detallada
                    </Form.Label>
                    <Form.Control
                      type="text"
                      name="inpuTitulo"
                      id="inpuTitulo"
                      placeholder="¿Escribe aqui tu pregunta? Sé especifico"
                      maxLength="200"
                      value={inputTitulo}
                      onChange={(e) => setInputTitulo(e.target.value)}
                      required
                    ></Form.Control>
                  </Form.Group>
                </Row>

                <Row className="mb-3">
                  <Form.Group as={Col} controlId="contenidoPregunta">
                    <h1>Cuerpo</h1>
                    <Form.Label>
                      Incluye toda los datos necesarios para responder tu pregunta
                    </Form.Label>
                    <Form.Control
                      as="textarea"
                      name="textarea_pregunta"
                      rows="20"
                      cols="100"
                      placeholder="Escribe con detalle tu consulta"
                      value={textarea_pregunta}
                      onChange={(e) => setTextarea_Pregunta(e.target.value)}
                      required
                      type="text"
                      id="inpuTitulo"
                      
                      style={{ height: "300px" }}
                    ></Form.Control>
                  </Form.Group>
                </Row>
                <Row className="mb-3">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Subir Imagen</span>
                    </div>
                    <div class="custom-file">
                      <input
                        class="custom-file-input"
                        type="file"
                        name="adjuntarImagen"
                        id="adjuntarImagen"
                      ></input>
                      <label class="custom-file-label" for="inputGroupFile01">
                        Elige una Imagen
                      </label>
                    </div>
                  </div>
                </Row>
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="contenidoPregunta">
                    <h1>Etiquetas</h1>
                    <Form.Label>
                      Editar las etiquetas (como minimo debe haber una o como maximo 5)
                    </Form.Label>
                    <Form.Control
                      type="text"
                      name="inputEtiqueta"
                      id="inputEtiqueta"
                      placeholder="ejemplo.: c#, javascript, array"
                      maxLength="100"
                      value={inputEtiqueta}
                      onChange={(e) => setInputEtiqueta(e.target.value)}
                      required
                    ></Form.Control>
                  </Form.Group>
                </Row>
                <Row className="mb-3" as={Col}>
                  <Button type="button" onClick={enviarPublicacion}>
                    Publicar tu pregunta
                  </Button>
                </Row>
              </Form>
            </div>
            <div class="col-md-4">
              <Card>
                <Card.Header>Como formular una pregunta</Card.Header>
                <ListGroup variant="flush">
                  <ListGroup.Item>
                    <p>Introduce un breve titulo de tu problema.</p>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <p> Incluya detalles sobre su objetivo.</p>
                    <p> Describe los resultados esperados y reales.</p>
                    <p>Incluye cualquier mensaje de error</p>
                    <p>Incluye una imagen como ayuda a resolver tu pregunta</p>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <p>
                      Introduce alguna etiqueta para saber que sobre que lenguaje es la pregunta.
                    </p>
                  </ListGroup.Item>
                </ListGroup>
              </Card>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CrearPublicacion;
