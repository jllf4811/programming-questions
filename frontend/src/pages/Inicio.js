import Cabecera from "../componentes/Cabecera";
import { Link } from "react-router-dom";
import "../style/EstilosPagina/Inicio.css";

const Inicio = (props) => {
  return (
    <>
      <header>
        <Cabecera />
      </header>
      <main className="contenido">
        <section>
          {localStorage.getItem("rol") === "estudiante" &&
          (window.location.pathname === "/" || window.location.pathname === "/pregunta/") ? (
            <>
              {/* <Link className="Link" to="/publicarPregunta">
                Formular pregunta
              </Link> */}
            </>
          ) : (
            ""
          )}
        </section>
        <section>{props.children}</section>
      </main>
    </>
  );
};

export default Inicio;
