const Footer = () => {

  return(
    
    
   
  <footer class="footer rounded-6">
    <div>
      <a href="https://coreui.io">CoreUI</a>
      <span>&copy; 2020 creativeLabs.</span>
    </div>
    <div class="ml-auto">
      <span>Powered by</span>
      <a href="https://coreui.io">CoreUI</a>
    </div>
  </footer>

    )
}

export default Footer;