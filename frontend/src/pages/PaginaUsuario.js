import Perfil from "../components/Perfil";
import { Link, Redirect } from "react-router-dom";
import { Button } from "react-bootstrap";
import "../style/EstilosPagina/paginaUsuario.css";

const PaginaUsuario = () => {
  return (
    <>
      <>
        <header></header>
        <main className="contenido">
          <section></section>
          <div>
            <Perfil />
          </div>
          {/* <div class="boton">
            <Button variant="dark" block size="lg">
              <Link to="/perfil/publicaciones">Editar tus publicaciones</Link>
            </Button>
          </div> */}
        </main>
      </>
    </>
  );
};

export default PaginaUsuario;
