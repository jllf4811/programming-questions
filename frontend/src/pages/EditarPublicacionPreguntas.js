import { useState, useContext } from "react";
import { useParams } from "react-router";
import { Link, useHistory } from "react-router-dom";
import Cabecera from "../componentes/Cabecera";
import { ContextoToken } from "../components/ProveedorToken";

import usePublicaciones from "../hooks/usePublicaciones";

import "../style/EstilosPagina/editarPublicacionPreguntas.css";

import { Button, Card, Col, Form, ListGroup, Row } from "react-bootstrap";

const EditarPublicacionPreguntas = () => {
  const { id } = useParams();
  const [publicaciones] = usePublicaciones();
  const [inputTitulo, setInputTitulo] = useState(publicaciones[id]?.titulo);
  const [inputEtiqueta, setInputEtiqueta] = useState("");
  const history = useHistory();
  const [textoTextarea, setTextoTextarea] = useState("");
  const [token] = useContext(ContextoToken);
  console.log(inputTitulo);
  const editarPregunta = async (e) => {
    if (textoTextarea) {
      const res = await fetch(
        `http://localhost:3050/pregunta/${publicaciones[id].id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            autorizado: token,
          },
          body: JSON.stringify({
            titulo: inputTitulo,
            texto: textoTextarea,
            tecnologiaLenguajes: inputEtiqueta,
            imagen: "",
          }),
        }
      );
      const json = await res.json();
      if (res.ok) {
        alert("se ha Editado corectamente la publicacion");
        redireccionar();
      } else {
        alert(json.error);
      }
    } else {
      alert("No has hecho ninguna modificación");
    }
  };

  const redireccionar = () => {
    history.push("/perfil/publicaciones");
  };

  return (
    <>

<Form class="formulario">
        <Row className="mb-2">
          <Form.Group as={Col} className="editar-publicacion-pregunta">
            <Form.Label>
              <h2>Titulo de la pregunta</h2>
            </Form.Label>
            <Link className="Link" to={`/pregunta/${id}`}>
              <h3 className="preguntaID">{publicaciones[id]?.titulo}</h3>
            </Link>
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>
              <h2>Contenido de la pregunta</h2>
            </Form.Label>
            <h3>{publicaciones[id]?.textoPregunta}</h3>
          </Form.Group>
        </Row>
        </Form>

      {/* <Form class="formulario">
        <Form.Label>
          <h2>Titulo de la pregunta</h2>
        </Form.Label>
        <Form.Group className="editar-publicacion-pregunta">
          <Form.Label>
            <Link className="Link" to={`/pregunta/${id}`}>
              <h3 className="preguntaID">{publicaciones[id]?.titulo}</h3>
            </Link>
          </Form.Label>
        </Form.Group>
        <Form.Group>
          <Form.Label>
            <h2>Contenido de la pregunta</h2>
          </Form.Label>
        </Form.Group>
        <Form.Label>{publicaciones[id]?.textoPregunta}</Form.Label>
      </Form> */}


      {/* <section className="editar-publicacion-pregunta">
        <article>
          <Link className="Link" to={`/pregunta/${id}`}>
            <h2 className="preguntaID">{publicaciones[id]?.titulo}</h2>
          </Link>
          <p>{publicaciones[id]?.textoPregunta}</p>
        </article>
      </section> */}

      <div class="principal">
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <Form method="PUT">
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="tituloPregunta">
                    <div>
                      <h1>Editar una pregunta</h1>
                    </div>
                    <div>
                      <h3>Titulo</h3>
                    </div>
                    <Form.Label>
                      Editar la pregunta ten encuenta formular la pregunta de
                      una forma clara y detallada
                    </Form.Label>
                    <Form.Control
                      type="text"
                      name="inpuTitulo"
                      id="inpuTitulo"
                      placeholder="¿Escribe aqui tu pregunta? Sé especifico"
                      maxLength="200"
                      value={inputTitulo}
                      onChange={(e) => setInputTitulo(e.target.value)}
                      required
                    ></Form.Control>
                  </Form.Group>
                </Row>
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="contenidoPregunta">
                    <div>
                      <h3>Cuerpo</h3>
                    </div>
                    <Form.Label>
                      Editar e Incluye toda los datos necesarios para que
                      respondedan tu pregunta
                    </Form.Label>
                    <Form.Control
                      as="textarea"
                      name="textarea_pregunta"
                      rows="20"
                      cols="100"
                      placeholder="Escribe con detalle tu consulta"
                      value={textoTextarea}
                      onChange={(e) => setTextoTextarea(e.target.value)}
                      required
                      type="text"
                      id="inpuTitulo"
                      style={{ height: "300px" }}
                    ></Form.Control>
                  </Form.Group>
                </Row>
                <Row className="mb-3">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Subir Imagen</span>
                    </div>
                    <div class="custom-file">
                      <input
                        class="custom-file-input"
                        type="file"
                        name="adjuntarImagen"
                        id="adjuntarImagen"
                      ></input>
                      <label class="custom-file-label" for="inputGroupFile01">
                        Elige una Imagen
                      </label>
                    </div>
                  </div>
                </Row>
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="contenidoPregunta">
                    <div>
                      <h3>Etiquetas</h3>
                    </div>
                    <Form.Label>
                      Editar las etiquetas (como minimo debe haber una o como
                      maximo 5)
                    </Form.Label>
                    <Form.Control
                      type="text"
                      name="inputEtiqueta"
                      id="inputEtiqueta"
                      placeholder="ejemplo.: c#, javascript, array"
                      maxLength="100"
                      value={inputEtiqueta}
                      onChange={(e) => setInputEtiqueta(e.target.value)}
                      required
                    ></Form.Control>
                  </Form.Group>
                </Row>
                <Row className="mb-3" as={Col}>
                  <Button type="button" onClick={editarPregunta}>
                    Editar tu pregunta
                  </Button>
                </Row>
              </Form>
            </div>

            <div class="col-md-4">
              <Card>
                <Card.Header>Como editar una pregunta</Card.Header>
                <ListGroup variant="flush">
                  <ListGroup.Item>
                    <p>Introduce un breve titulo de tu problema.</p>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <p> Incluya detalles sobre su objetivo.</p>
                    <p> Describe los resultados esperados y reales.</p>
                    <p>Incluye cualquier mensaje de error</p>
                    <p>Incluye una imagen como ayuda a resolver tu pregunta</p>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <p>
                      Introduce alguna etiqueta para saber que sobre que
                      lenguaje es la pregunta.
                    </p>
                  </ListGroup.Item>
                </ListGroup>
              </Card>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditarPublicacionPreguntas;
