import { useState, useContext } from "react";
import { useParams } from "react-router";
import { Link, useHistory } from "react-router-dom";
import { ContextoToken } from "../components/ProveedorToken";
import { Button, Card, Col, Form, ListGroup, Row } from "react-bootstrap";
import usePublicaciones from "../hooks/usePublicaciones";
import "../style/EstilosPagina/editarPublicacionRespuestas.css";

const EditarPublicacionRespuestas = () => {
  const [publicaciones] = usePublicaciones();
  const [token] = useContext(ContextoToken);
  let history = useHistory();
  const { id } = useParams();
  const [respuesta, setRespuesta] = useState(publicaciones[id]?.textoRespuesta);

  const editarRespuestas = async (e) => {
    if (respuesta) {
      const res = await fetch(
        `http://localhost:3050/pregunta/respuesta/${publicaciones[id].id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            autorizado: token,
          },
          body: JSON.stringify({ texto: respuesta }),
        }
      );
      const json = await res.json();
      if (res.ok) {
        alert("se ha Editado corectamente la publicacion");
        redireccionar();
      } else {
        alert(json.error);
      }
    } else {
      alert("No has hecho ninguna modificación");
    }
  };

  const redireccionar = () => {
    history.push("/perfil/publicaciones");
  };

  return (
    <>
      <Form class="formulario">
        <Row className="mb-2">
          <Form.Group as={Col}>
            <Form.Label>
              <h2>Titulo de la pregunta</h2>
            </Form.Label>
            <Link
              className="Link"
              to={`/pregunta/${publicaciones[id]?.id_pregunta}`}
            >
              <h3 className="preguntaID">{publicaciones[id]?.titulo}</h3>
            </Link>
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>
              <h2>Contenido de la pregunta</h2>
            </Form.Label>
            <h3>{publicaciones[id]?.textoPregunta}</h3>
          </Form.Group>
        </Row>
        <Row className="mb-2">
          <Form.Group className="editar-publicacion-respuestas" as={Col}>
            <Form.Label>
              <h2>Contenido de la respuesta</h2>
            </Form.Label>
            <h5>{publicaciones[id]?.textoRespuesta}</h5>
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>
              <h2>Fecha de publicacion</h2>
              <time>{publicaciones[id]?.fechaPublicacionRespuestas}</time>
            </Form.Label>
            <Form.Label className="fechaActualizacion">
              <h2>Fecha de actualizacion</h2>{" "}
              <time>{publicaciones[id]?.fechaActualizacionRespuestas}</time>
            </Form.Label>
          </Form.Group>
        </Row>

        {/* <Form.Group className="editar-publicacion-pregunta">
          <Form.Label>
            <Link
              className="Link"
              to={`/pregunta/${publicaciones[id]?.id_pregunta}`}
            >
              <h3 className="preguntaID">{publicaciones[id]?.titulo}</h3>
            </Link>
          </Form.Label>
        </Form.Group> */}
        {/* <Form.Group>
          <Form.Label>
            <h2>Contenido de la respuesta</h2>
          </Form.Label>
        </Form.Group>
        <Form.Label>{publicaciones[id]?.textoPregunta}</Form.Label> */}
      </Form>

      {/* <section className="editar-publicacion-pregunta">
        <article>
          <Link
            className="Link"
            to={`/pregunta/${publicaciones[id]?.id_pregunta}`}
          >
            <h2 className="preguntaID">{publicaciones[id]?.titulo}</h2>
          </Link>
          <p>{publicaciones[id]?.textoPregunta}</p>
        </article>
      </section> */}
      <div class="principal">
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <Form method="PUT">
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="tituloPregunta">
                    <div>
                      <h1>Haz los cambios necesarios en la respuesta</h1>
                    </div>

                    {/* <Form.Label>
                      Editar la respuesta ten encuenta formular la pregunta de
                      una forma clara y detallada
                    </Form.Label> */}
                    <Form.Control
                      as="textarea"
                      name="textarea_pregunta"
                      rows="20"
                      cols="100"
                      placeholder="Escribe tu respuesta"
                      onChange={(e) => setRespuesta(e.target.value)}
                      required
                      style={{ height: "300px" }}
                    ></Form.Control>
                  </Form.Group>
                </Row>

                <Row className="mb-3" as={Col}>
                  <Button type="button" onClick={editarRespuestas}>
                    Editar tu respuesta
                  </Button>
                </Row>
              </Form>
            </div>

            <div class="col-md-4">
              <Card>
                <Card.Header>Como editar una respuesta</Card.Header>
                <ListGroup variant="flush">
                  <ListGroup.Item>
                    <p>Puedes ver el titulo del la pregunta.</p>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <p> Se muestra el contenido de la pregunta.</p>
                  </ListGroup.Item>
                  <ListGroup.Item>
                    <p>
                      Se muestran la fecha de actuallizacion de la respuesta y
                      la fecha de publicacion.
                    </p>
                  </ListGroup.Item>
                </ListGroup>
              </Card>
            </div>
          </div>
        </div>
      </div>

      {/* <section className="editar-publicacion-respuestas">
        <article>
          <p>{publicaciones[id]?.textoRespuesta}</p>
          <time>{publicaciones[id]?.fechaActualizacionRespuestas}</time>
          <time>{publicaciones[id]?.fechaPublicacionRespuestas}</time>
        </article>
      </section> */}

      {/* <section>
        <form>
          <section>
            <h3>Haz los cambios necesarios en la respuesta</h3>
            <textarea
              name="textarea_pregunta"
              rows="20"
              cols="100"
              placeholder="Escribe con detalle tu consulta"
              onChange={(e) => setRespuesta(e.target.value)}
              required
            ></textarea>
            <button type="button" onClick={editarRespuestas}>
              Editar Publicación
            </button>
          </section>
        </form>
      </section> */}
    </>
  );
};

export default EditarPublicacionRespuestas;
