import FormularioRegistro from "../components/FormularioRegistro";
import { useContext } from "react";
import { ContextoToken } from "../components/ProveedorToken";
import { Redirect, Link } from "react-router-dom";
import "../style/EstilosPagina/PaginaRegistro.css";

const PaginaRegistro = (props) => {
  const [token] = useContext(ContextoToken);

  return (
    <>
      {!token ? (
        <div class="principal">
          <h1>Registro</h1>

          <FormularioRegistro />

          <p>
            ¿Ya tienes cuenta?
            <Link to="/login" style={{ fontWeight: "bold" }}>
              Inicio de Sesión
            </Link>
          </p>
        </div>
      ) : (
        <Redirect to="/" />
      )}
    </>
  );
};

export default PaginaRegistro;
