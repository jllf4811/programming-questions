const { getConnection } = require("../db");

async function esExperto(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    if (req.autorizar.rol !== "experto" && req.autorizar.rol !== "administrador") {
      throw new Error(
        "no puedes realizar esa accion solo los usuarios expertos o administrador tienen permisos para realizarlo"
      );
    }
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { esExperto };
