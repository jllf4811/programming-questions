const jsonwebtoken = require("jsonwebtoken");
const { getConnection } = require("../db");

async function estaAutorizado(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { autorizado } = req.headers;

    if (!autorizado) {
      throw new Error("no tienes token");
    }

    let tokenInfo;
    try {
      tokenInfo = jsonwebtoken.verify(autorizado, process.env.SECRET);
    } catch (error) {
      console.error(error);
      throw new Error("no se ha podido verificar token");
    }

    let usuario;
    try {
      [usuario] = await connection.query(
        `
      SELECT id
      FROM usuarios
      WHERE id=?
      `,
        [tokenInfo.id]
      );
    } catch (error) {
      throw new Error("El usuario no se ha podido encontrar");
    }

    if (usuario.length < 1) {
      throw new Error("el usuarios del token no se ha encontrado");
    }

    req.autorizar = tokenInfo;

    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { estaAutorizado };
