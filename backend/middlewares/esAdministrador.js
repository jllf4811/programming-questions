const { getConnection } = require("../db");

async function esAdministrador(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    if (req.autorizar.rol !== "administrador") {
      throw new Error(
        "no puedes realizar esa accion solo los usuarios administrador tienen permisos para realizarlo"
      );
    }
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { esAdministrador };
