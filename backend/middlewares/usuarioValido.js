const { getConnection } = require("../db");

async function usuarioValido(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { id } = req.params;

    if (req.autorizar.id !== Number(id)) {
      throw new Error("no puedes editar esa publicacion");
    }

    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { usuarioValido };
