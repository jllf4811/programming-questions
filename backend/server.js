require("dotenv").config();
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const fileUpload = require("express-fileupload");

const app = express();

const port = process.env.MYSQL_PORT;

const { crearUsuario } = require("./Controladores/usuario/crearUsuario");
const { listarUsuarios } = require("./Controladores/usuario/listarUsuarios");
const { iniciarSesion } = require("./Controladores/usuario/iniciarSesion");
const { activarUsuario } = require("./Controladores/usuario/activarUsuario");
const { desactivarUsuario } = require("./Controladores/usuario/desactivarUsuario");
const { eliminarUsuario } = require("./Controladores/usuario/eliminarUsuario");
const { editarUsuario } = require("./Controladores/usuario/editarUsuario");
const { editarContrasena } = require("./Controladores/usuario/editarContrasena");
//const { reiniciarContrasena } = require("./Controladores/usuario/reiniciarContrasena");
const { crearPregunta } = require("./Controladores/Pregunta/crearPregunta");
const { editarPregunta } = require("./Controladores/Pregunta/editarPregunta");
const { listarPregunta } = require("./Controladores/Pregunta/listarPregunta");
const { eliminarPregunta } = require("./Controladores/Pregunta/eliminarPregunta");
const { crearRespuesta } = require("./Controladores/Respuestas/crearRespuesta");
const { editarRespuesta } = require("./Controladores/Respuestas/editarRespuesta");
const { eliminarRespuesta } = require("./Controladores/Respuestas/eliminarRespuesta");
const { listarRespuestas } = require("./Controladores/Respuestas/listarRespuestas");
const { votar } = require("./Controladores/Respuestas/Votar");
const { tecnologiaLenguajes } = require("./Controladores/usuario/tecnologiasLenguajes");
const { listarPublicaciones } = require("./Controladores/listarPublicaciones");
const { olvidarContrasena } = require("./Controladores/usuario/olvidarContraseña");
const { estaAutorizado } = require("./middlewares/estaAutorizado");

const { esExperto } = require("./middlewares/esExperto");
const { esEstudiante } = require("./middlewares/esEstudiante");
const { usuarioValido } = require("./middlewares/usuarioValido");
const { esAdministrador } = require("./middlewares/esAdministrador");

app.use(cors());
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(fileUpload());
app.post("/usuario", crearUsuario, tecnologiaLenguajes);
app.get("/usuario/:id", listarUsuarios);
app.put("/activar/:codigoRegistro", activarUsuario);
//app.put("/usuario/:id", estaAutorizado, usuarioValido, desactivarUsuario);
app.post("/iniciarsesion", iniciarSesion);
app.delete("/usuario/:id", estaAutorizado, usuarioValido, esAdministrador, eliminarUsuario);
app.put("/usuario/editar/:id", estaAutorizado, usuarioValido, editarUsuario);
//app.put("/reiniciar/:codigoRegistro", reiniciarContrasena);
app.post("/recuperar", olvidarContrasena);
app.put("/usuario/contrasena/:id", estaAutorizado, usuarioValido, editarContrasena);

app.post("/pregunta", estaAutorizado, esEstudiante, crearPregunta);
app.put("/pregunta/:id", estaAutorizado, esEstudiante, editarPregunta);
app.get("/pregunta", listarPregunta);
app.delete("/pregunta/:id", estaAutorizado, esEstudiante, eliminarPregunta);

app.get("/perfil/publicaciones", estaAutorizado, listarPublicaciones);

app.post("/pregunta/:id/respuesta", estaAutorizado, esExperto, crearRespuesta);
app.put("/pregunta/respuesta/:id", estaAutorizado, esExperto, editarRespuesta);
app.get("/pregunta/:id/respuesta", listarRespuestas);
app.delete("/pregunta/respuesta/:id", estaAutorizado, esExperto, eliminarRespuesta);
app.put("/respuesta/:id/voto", estaAutorizado, votar);

app.use(function (error, req, res, next) {
  if (error) {
    res.status(error.httpStatus || 500).send({ error: error.message });
  }
});

app.use(function (req, res) {
  res.send("ruta no encontrada");
});

app.listen(port, () => {
  console.log(`Servidor puerto ${port}`);
});
