const { getConnection } = require("../../db");

async function votar(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { voto } = req.body;
    const { id } = req.params;

    if (!voto || !id) {
      throw new error("faltan datos de ingreso");
    }
    const votosValidos = ["1", "-1"];
    if (!votosValidos.includes(voto)) {
      throw new Error("error en los valores de los votos");
    }

    let respuesta;
    try {
      [respuesta] = await connection.query(
        `
      SELECT id
      FROM respuestas 
      WHERE id=? 
      `,
        [id]
      );
    } catch (error) {
      throw new Error("no se ha encontrado la respuesta podido realizar el voto");
    }

    if (respuesta.length < 1) {
      throw new Error("no existe la respuesta");
    }

    const [existeValoracion] = await connection.query(
      `
        SELECT id_usuario, id_respuesta
        FROM valoracion
        WHERE id_usuario=? AND id_respuesta=?
        `,
      [req.autorizar.id, respuesta[0].id]
    );

    console.log(existeValoracion.length < 1);
    if (existeValoracion.length < 1) {
      console.log(req.autorizar.id);
      try {
        await connection.query(
          `
        INSERT INTO valoracion(voto, id_respuesta, id_usuario)
        VALUES(?,?,?)
        `,
          [voto, respuesta[0].id, req.autorizar.id]
        );
      } catch (error) {
        console.error(error);
        throw new Error("no se ha podido votar");
      }
    } else {
      console.log("entro");
      await connection.query(
        `
        UPDATE valoracion 
        SET voto=?
        WHERE id_usuario=? AND id_respuesta=?
      `,
        [voto, req.autorizar.id, respuesta[0].id]
      );
    }

    res.send({
      status: "ok",
      message: "has votado",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}
module.exports = { votar };
