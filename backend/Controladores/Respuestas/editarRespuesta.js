const { getConnection } = require("../../db");

async function editarRespuesta(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { id } = req.params;
    const { texto } = req.body;

    if (!id && !id_pregunta) {
      throw new Error("falta parametro de busqueda");
    }
    if (!texto) {
      throw new Error("no puede estar vacio la respuesta");
    }
    let existeRespuesta;
    try {
      [existeRespuesta] = await connection.query(
        `
        SELECT id, id_usuario
        FROM respuestas
        WHERE  id=?
      `,
        [id]
      );
    } catch (error) {
      throw new Error("problema en la busqueda de la respuesta");
    }
    console.log(existeRespuesta[0], req.autorizar.id);
    if (existeRespuesta.length < 1) {
      throw new Error("no se ha encontrado la respuesta");
    }

    if (
      req.autorizar.id !== existeRespuesta[0].id_usuario &&
      req.autorizar.rol !== "administrador"
    ) {
      throw new Error("no puedes editar la respuestas");
    }

    try {
      await connection.query(
        `
        UPDATE respuestas
        SET textoRespuesta=?, fechaActualizacionRespuestas=?
        WHERE id=?
      `,
        [texto, new Date(), id]
      );
    } catch (error) {
      throw new Error("no se ha encontrado la respuesta");
    }

    res.send({
      status: "ok",
      message: "respuesta editada",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { editarRespuesta };
