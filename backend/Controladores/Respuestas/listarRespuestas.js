const { getConnection } = require("../../db");

async function listarRespuestas(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { autorizado } = req.headers;
    const { id } = req.params;

    let pregunta;
    try {
      [pregunta] = await connection.query(
        `
        SELECT p.*,u.nombre, (SELECT DISTINCT pt.id_pregunta) AS id_pregunta, GROUP_CONCAT(t.tecnologiaLenguajes) AS etiqueta
        FROM preguntas_tecnologias pt
        INNER JOIN tecnologias t ON pt.id_tecnologia=t.id
        INNER JOIN preguntas p ON p.id=pt.id_pregunta
        INNER JOIN usuarios u ON u.id=p.id_usuario
        WHERE p.id=?
        GROUP BY p.id
        ORDER BY fechaPublicacionPregunta DESC
      `,
        [id]
      );
    } catch (error) {
      throw new Error("no se pudo obtener las respuestas");
    }

    if (pregunta.length < 1) {
      throw new Error("la pregunta no se encontro");
    }

    let respuestas;
    if (true) {
      try {
        [respuestas] = await connection.query(
          `
          SELECT respuestas.*, COUNT(valoracion.voto) AS votos, 
          SUM(CASE WHEN valoracion.voto = '1' THEN 1 ELSE 0 END) AS votos_positivos,
          SUM(CASE WHEN valoracion.voto = '-1' THEN -1 ELSE 0 END) AS votos_negativos,
          usuarios.nombre, usuarios.avatar
          FROM respuestas
          INNER JOIN usuarios ON id_usuario=usuarios.id
          LEFT JOIN valoracion ON respuestas.id = valoracion.id_respuesta 
          WHERE id_pregunta=? 
          GROUP BY respuestas.id
          ORDER BY fechaPublicacionRespuestas DESC;
          `,
          [pregunta[0].id]
        );
      } catch (error) {
        throw new Error("no se ha podido encontrar respuestas");
      }
      const datos = respuestas.map((item) => item);
      pregunta[0].listasDeRespuestas = datos;
    }

    res.send(pregunta[0]);
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { listarRespuestas };
