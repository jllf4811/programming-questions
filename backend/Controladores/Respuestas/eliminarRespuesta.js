const { getConnection } = require("../../db");

async function eliminarRespuesta(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { id } = req.params;

    let existeRespuesta;
    try {
      [existeRespuesta] = await connection.query(
        `
      SELECT id, id_usuario
      FROM respuestas
      WHERE id=?
      `,
        [id]
      );
    } catch (error) {
      throw new Error("no se encontro la respuesta");
    }

    if (existeRespuesta.length < 1) {
      throw new Error("no existe la respuesta");
    }

    if (
      existeRespuesta[0].id_usuario !== req.autorizar.id &&
      req.autorizar.rol !== "administrador"
    ) {
      throw new Error("no eres el usuario que ha creado la respuesta");
    }

    try {
      await connection.query(
        `
        DELETE FROM respuestas WHERE id=?
      `,
        [id]
      );
    } catch (error) {
      throw new Error("no se ha podido borrar");
    }
    res.send({
      status: "ok",
      message: "La respuesta ha sido eliminada",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { eliminarRespuesta };
