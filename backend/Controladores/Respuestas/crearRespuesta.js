const { getConnection } = require("../../db");

async function crearRespuesta(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { texto } = req.body;
    const { id } = req.params;

    if (!texto) {
      throw new Error("No contiene texto");
    }
    const [existeRespuesta] = await connection.query(
      `
      SELECT id
      FROM preguntas
      WHERE id=?
      `,
      [id]
    );

    if (existeRespuesta.length < 1) {
      throw new Error("no existe la pregunta");
    }

    try {
      await connection.query(
        `
      INSERT INTO respuestas(
        textoRespuesta,
        fechaPublicacionRespuestas,
        fechaActualizacionRespuestas,
        id_usuario,
        id_pregunta
        )
      VALUES(?,?,?,?,?)
      `,
        [texto, new Date(), new Date(), req.autorizar.id, id]
      );
    } catch (error) {
      throw new Error("ha surgido un error en la respuesta");
    }

    res.send({
      status: "ok",
      message: "Respuesta creada",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { crearRespuesta };
