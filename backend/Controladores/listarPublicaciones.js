const { getConnection } = require("../db");

async function listarPublicaciones(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    let datos;

    try {
      if (req.autorizar.rol === "estudiante") {
        [datos] = await connection.query(
          `
        SELECT p.*, u.rol
        FROM preguntas p
        INNER JOIN usuarios u ON u.id=p.id_usuario
        WHERE p.id_usuario=?
        `,
          [req.autorizar.id]
        );
      }
      if (req.autorizar.rol === "experto") {
        [datos] = await connection.query(
          `
          SELECT  p.*,r.*, u.rol
          FROM respuestas r
          INNER JOIN preguntas p ON p.id=r.id_pregunta
          INNER JOIN usuarios u ON u.id=r.id_usuario
          WHERE r.id_usuario=?
        `,
          [req.autorizar.id]
        );
      }
    } catch (error) {
      throw new Error("No se ha podido obtener las publicaciones");
    }

    res.send(datos);
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { listarPublicaciones };
