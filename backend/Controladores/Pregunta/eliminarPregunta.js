const { getConnection } = require("../../db");

async function eliminarPregunta(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { id } = req.params;
    //no funciona
    let existePregunta;
    try {
      [existePregunta] = await connection.query(
        `
      SELECT id, id_usuario
      FROM preguntas
      WHERE id=?
      `,
        [id]
      );
    } catch (error) {
      throw new Error("no se encontro la pregunta");
    }

    if (existePregunta.length < 1) {
      throw new Error("no existe la pregunta");
    }

    if (
      req.autorizar.id !== existePregunta[0].id_usuario &&
      req.autorizar.rol !== "administrador"
    ) {
      throw new Error("no puedes editar la respuestas");
    }

    try {
      await connection.query(
        `
        DELETE FROM preguntas WHERE id=?
      `,
        [id]
      );
    } catch (error) {
      throw new Error("no se ha podido borrar");
    }

    res.send({
      status: "ok",
      message: "pregunta eliminado",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { eliminarPregunta };
