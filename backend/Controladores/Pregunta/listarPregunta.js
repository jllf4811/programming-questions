const { getConnection } = require("../../db");

async function listarPregunta(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { buscar, filtro, sinRespuestas } = req.query;

    //{{url}}/pregunta?buscar=&filtro=&sinRespuesta
    //añadir etiquetas, mejorar busqueda y filtros
    let datos;
    if (buscar || filtro || sinRespuestas) {
      [datos] = await connection.query(
        `
        SELECT p.*,u.nombre ,(SELECT DISTINCT pt.id_pregunta) AS id_p, b.id_pregunta, b.filtro,  GROUP_CONCAT(t.tecnologiaLenguajes  ) AS etiqueta
        FROM preguntas_tecnologias pt
        INNER JOIN (SELECT (SELECT DISTINCT pt.id_pregunta ) AS id_pregunta,
              GROUP_CONCAT(t.tecnologiaLenguajes  ) AS filtro
              FROM preguntas_tecnologias pt
              INNER JOIN tecnologias t ON pt.id_tecnologia=t.id
                    WHERE t.tecnologiaLenguajes LIKE CONCAT ('%', ?)
              GROUP BY id_pregunta
                    ) AS b ON b.id_pregunta=pt.id_pregunta
        INNER JOIN tecnologias t ON t.id=pt.id_tecnologia
        INNER JOIN preguntas p ON pt.id_pregunta=p.id
        INNER JOIN usuarios u ON u.id=p.id_usuario
        LEFT JOIN respuestas r ON r.id_pregunta=p.id
        WHERE ${buscar ? "MATCH (p.titulo) AGAINST (?)" : "p.titulo LIKE CONCAT ('%',?,'%')"} ${
          sinRespuestas ? " AND r.id_pregunta IS NULL" : ""
        }
        GROUP BY p.id
            `,
        [filtro, buscar]
      );
    } else {
      try {
        [datos] = await connection.query(`
        SELECT p.*,u.nombre, (SELECT DISTINCT pt.id_pregunta) AS id_pregunta, GROUP_CONCAT(t.tecnologiaLenguajes) AS etiqueta
        FROM preguntas_tecnologias pt
        INNER JOIN tecnologias t ON pt.id_tecnologia=t.id
        INNER JOIN preguntas p ON p.id=pt.id_pregunta
        INNER JOIN usuarios u ON u.id=p.id_usuario
        GROUP BY p.id
        ORDER BY fechaPublicacionPregunta DESC
        `);
      } catch (error) {
        throw new Error("ha ocurrido un error en la busqueda");
      }
    }

    const preguntas = datos.map((item) => item);

    res.send(preguntas);
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { listarPregunta };
