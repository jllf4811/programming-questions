const { getConnection } = require("../../db");
const { subidasImagenes } = require("../../helpers");

async function editarPregunta(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { id } = req.params;

    const { titulo, texto, tecnologiaLenguajes } = req.body;
    if (!titulo && !texto) {
      throw new Error("Parametros titulo y texno no pueden estar vacios");
    }

    let arrayTecnologiaLenguajes = tecnologiaLenguajes.toLowerCase().split(",");

    if (!tecnologiaLenguajes || arrayTecnologiaLenguajes.length > 5) {
      throw new Error("tecnologiaLenguajes el parametro no deve estar vacio o tener mas de 5 tags");
    }

    if (!id) {
      throw new Error("falta parametro de busqueda");
    }

    let existePregunta;
    try {
      [existePregunta] = await connection.query(
        `
        SELECT id, id_usuario
        FROM preguntas
        WHERE id=?
      `,
        [id]
      );
    } catch (error) {
      throw new Error("problema en la busqueda de la pregunta");
    }

    if (existePregunta.length < 1) {
      throw new Error("no se ha encontrado la pregunta");
    }

    if (
      req.autorizar.id !== existePregunta[0].id_usuario &&
      req.autorizar.rol !== "administrador"
    ) {
      throw new Error("no puedes editar la pregunta");
    }

    let nombreImagen;
    try {
      const [[contieneImagen]] = await connection.query(
        `
        SELECT imagen
        FROM preguntas
        WHERE id=?
        `,
        [id]
      );
      nombreImagen = contieneImagen.imagen;
    } catch (error) {
      next(error);
    }

    if (req.files && req.files.imagen) {
      nombreImagen = await subidasImagenes({
        archivo: req.files.imagen,
        directorio: "Imagenes",
      });
    }

    try {
      await connection.query(
        `
        UPDATE preguntas
        SET titulo=?, textoPregunta=?, fechaActualizacionPregunta=?, imagen=?
        WHERE id=?
      `,
        [titulo, texto, new Date(), nombreImagen, id]
      );
    } catch (error) {
      throw new Error("no se ha encontrado la pregunta");
    }

    for (const valor of arrayTecnologiaLenguajes) {
      await connection.query(
        `
    INSERT IGNORE INTO tecnologias(tecnologiaLenguajes)
    VALUES("${valor.trim()}");
    `
      );
    }

    let arrayIdTecnolgias = [];
    for (const valor of arrayTecnologiaLenguajes) {
      let [objeto] = await connection.query(
        `
        SELECT id, tecnologiaLenguajes
        FROM tecnologias
        WHERE tecnologiaLenguajes=?
        `,
        [valor.trim()]
      );

      arrayIdTecnolgias.push(objeto[0].id);
    }
    console.log();
    try {
      await connection.query(
        `
      DELETE FROM preguntas_tecnologias WHERE id_pregunta=?
    `,
        [id]
      );
    } catch (error) {
      throw new Error("borrados error etiqueta usuario");
    }

    for (const valor of arrayIdTecnolgias) {
      await connection.query(
        `
      INSERT INTO preguntas_tecnologias(id_pregunta, id_tecnologia)
      VALUES(?,?);
      `,
        [id, valor]
      );
    }

    res.send({
      status: "ok",
      message: "Pregunta editada",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { editarPregunta };
