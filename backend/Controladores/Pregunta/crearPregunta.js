const { getConnection } = require("../../db");
const { subidasImagenes } = require("../../helpers");

async function crearPregunta(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { titulo, texto, tecnologiaLenguajes } = req.body;
    if (!titulo || !texto || !tecnologiaLenguajes) {
      throw new Error("los apartados titulo, texto o etiqueta estan vacías");
    }

    let arrayTecnologiaLenguajes = tecnologiaLenguajes.toLowerCase().split(",");

    if (arrayTecnologiaLenguajes.length > 5) {
      throw new Error(
        "Solo se puede añadir como maximo 5 tecnologia/lenguajes en el que te especializas"
      );
    }

    let tituloExiste;
    try {
      [tituloExiste] = await connection.query(
        `
      SELECT titulo
      FROM preguntas
      WHERE titulo=?
      `,
        [titulo]
      );
    } catch (error) {
      next(error);
    }

    if (tituloExiste.length > 0) {
      throw new Error("Ya exite una publicación con esa pregunta");
    }

    let nombreImagen;
    if (req.files && req.files.imagen) {
      nombreImagen = await subidasImagenes({
        archivo: req.files.imagen,
        directorio: "Imagenes",
      });
    }

    try {
      await connection.query(
        `
      INSERT INTO preguntas(
        id_usuario,
        titulo,
        textoPregunta,
        imagen,
        fechaPublicacionPregunta,
        fechaActualizacionPregunta

      )VALUES(?,?,?,?,?,?);
      `,
        [req.autorizar.id, titulo, texto, nombreImagen, new Date(), new Date()]
      );
    } catch (error) {
      next(error);
    }

    let pregunta;
    try {
      [pregunta] = await connection.query(
        `
    SELECT id_usuario, id
    FROM preguntas
    WHERE id_usuario=? 
    ORDER BY fechaPublicacionPregunta DESC
    `,
        [req.autorizar.id]
      );
    } catch (error) {
      throw new Error("no se ha podido encontrar email");
    }

    if (pregunta < 1) {
      throw new Error("pregunta no encontrada");
    }

    for (const valor of arrayTecnologiaLenguajes) {
      await connection.query(
        `
    INSERT IGNORE INTO tecnologias(tecnologiaLenguajes)
    VALUES("${valor.trim()}");
    `
      );
    }

    let arrayIdTecnolgias = [];
    for (const valor of arrayTecnologiaLenguajes) {
      let [objeto] = await connection.query(
        `
        SELECT id, tecnologiaLenguajes
        FROM tecnologias
        WHERE tecnologiaLenguajes=?
        `,
        [valor.trim()]
      );

      arrayIdTecnolgias.push(objeto[0].id);
    }

    for (const valor of arrayIdTecnolgias) {
      await connection.query(
        `
      INSERT INTO preguntas_tecnologias(id_pregunta, id_tecnologia)
      VALUES(?,?);
      `,
        [pregunta[0].id, valor]
      );
    }

    res.send({
      status: "ok",
      message: "pregunta creada",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { crearPregunta };
