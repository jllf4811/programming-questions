const { getConnection } = require("../../db");

async function tecnologiaLenguajes(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { tecnologiaLenguajes, email, rol } = req.body;

    if (rol !== "estudiante" && rol !== "administrador") {
      if (!tecnologiaLenguajes) {
        throw new Error("falta parametro tecnologias/lenguajes en el que te especializas");
      }

      let arrayTecnologiaLenguajes = tecnologiaLenguajes.toLowerCase().split(",");

      if (arrayTecnologiaLenguajes.length > 5) {
        throw new Error(
          "Solo se puede añadir como maximo 5 tecnologia/lenguajes en el que te especializas"
        );
      }

      let usuarios;
      try {
        [usuarios] = await connection.query(
          `
      SELECT id
      FROM usuarios
      WHERE email=? 
      `,
          [email]
        );
      } catch (error) {
        throw new Error("no se ha podido encontrar email");
      }

      if (usuarios < 1) {
        throw new Error("usuario no encontrado");
      }

      for (const valor of arrayTecnologiaLenguajes) {
        await connection.query(
          `
        INSERT IGNORE INTO tecnologias(tecnologiaLenguajes)
        VALUES("${valor.trim()}");
        `
        );
      }

      let arrayIdTecnolgias = [];
      for (const valor of arrayTecnologiaLenguajes) {
        let [objeto] = await connection.query(
          `
        SELECT id, tecnologiaLenguajes
        FROM tecnologias
        WHERE tecnologiaLenguajes=?
        `,
          [valor.trim()]
        );
        arrayIdTecnolgias.push(objeto[0].id);
      }

      for (const valor of arrayIdTecnolgias) {
        await connection.query(
          `
        INSERT INTO usuarios_tecnologias(id_usuario, id_tecnologia)
        VALUES(?,?);
        `,
          [usuarios[0].id, valor]
        );
      }
    }
    res.send({
      status: "ok",
      message: "El usuario se ha creado sin ningun inconveniente",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { tecnologiaLenguajes };
