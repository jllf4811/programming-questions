const { getConnection } = require("../../db");
const { enviarEmail } = require("../../helpers");
const bcrypt = require("bcrypt");
const crypto = require("crypto");

async function crearUsuario(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    const { nombre, apellidos, email, contrasena, rol, tecnologiaLenguajes } = req.body;

    if (rol === "experto") {
      if (!tecnologiaLenguajes) {
        throw new Error("los expertos tienen que rellenar el parametro tecnologia/lenguajes");
      }
    }

    if (!nombre || !email || !contrasena || !rol) {
      throw new Error("faltan parametros sin cubrir que no pueden estar en blanco");
    }
    let existeEmail;
    try {
      [existeEmail] = await connection.query(
        `
      SELECT email, activar, codigoRegistro
      FROM usuarios
      WHERE email=?
      `,
        [email]
      );
    } catch (error) {
      next(error);
    }

    if (existeEmail.length > 0) {
      throw new Error("El email ya existe");
    }

    const contrasenaCodificada = await bcrypt.hash(contrasena, 10);
    const codigoRegistro = crypto.randomBytes(20).toString("hex").slice(0, 20);
    console.log(codigoRegistro, contrasenaCodificada, email);

    try {
      await connection.query(
        `
        INSERT INTO usuarios(
          nombre,
          email,
          contraseña,
          rol,
          codigoRegistro,
          fechaRegistro,
          fechaActualizacion
        )
        VALUES(?,?,?,?,?,?,?)
        `,
        [nombre, email, contrasenaCodificada, rol, codigoRegistro, new Date(), new Date()]
      );
    } catch (error) {
      console.error(error);
      throw new Error("no se ha podido insertar usuario");
    }
    const enlaceRegistro = `${process.env.DOMAIN}/activar/${codigoRegistro}`;

    await enviarEmail({
      to: email,
      subject: "Te registrate programmingQuestions",
      text: `pulsa el siguiente link para activar tu usuario:
      ${enlaceRegistro}`,
    });

    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}
module.exports = { crearUsuario };
