const { getConnection } = require("../../db");

async function listarUsuarios(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { id } = req.params;

    let usuario;

    try {
      [usuario] = await connection.query(
        `
                SELECT *
                FROM usuarios
                WHERE id=?
                `,
        [id]
      );
    } catch (error) {
      throw new Error("no se ha podido obtener usuario");
    }
    if (usuario.length < 1) {
      throw new Error("el usuario no existe en la base de datos");
    }

    res.send(usuario);
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { listarUsuarios };
