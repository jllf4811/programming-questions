const { getConnection } = require("../../db");
const bcrypt = require("bcrypt");

async function editarUsuario(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { nombre, email, contrasena } = req.body;

    if (nombre) {
      try {
        await connection.query(
          `
                UPDATE usuarios
                SET nombre=?
                WHERE id=?
                `,
          [nombre, req.autorizar.id]
        );
      } catch (error) {
        console.error(error);
        throw new Error("Ha habido un error al actualizar tu usuario");
      }
    }

    if (email && contrasena) {
      if (!contrasena) {
        throw new Error("falta contraseña");
      }

      let usuarioExistente;
      try {
        [usuarioExistente] = await connection.query(
          `
                SELECT *
                FROM usuarios
                WHERE id=?
                `,
          [req.autorizar.id]
        );
      } catch (error) {
        throw new Error("No se pudo buscar al usuario en la base de datos");
      }

      const contrasenaCodificada = usuarioExistente[0].contraseña;
      contrasenaBd = await bcrypt.hash(contrasena, 10);
      console.log(contrasenaCodificada, contrasenaBd);
      try {
        const isValid = await bcrypt.compare(contrasena, contrasenaCodificada);
        if (isValid === false) {
          throw new Error(
            "La contraseña introducida no coincide con la depositada en base de datos"
          );
        }
      } catch (error) {
        throw new Error("no se han podido comparar");
      }
      try {
        await connection.query(
          `
                  UPDATE usuarios
                  SET email=?
                  WHERE id=?
                  `,
          [email, req.autorizar.id]
        );
      } catch (error) {
        console.error(error);
        throw new Error("Ha habido un error al actualizar tu usuario");
      }
    }

    res.send("tu usuario ha sido actualizo satisfactoriamente");
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { editarUsuario };
