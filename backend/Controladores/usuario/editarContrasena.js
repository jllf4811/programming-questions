const { getConnection } = require("../../db");
const bcrypt = require("bcrypt");

async function editarContrasena(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { contrasenaVieja, nuevaContrasena, nuevaContrasena2 } = req.body;

    const { id } = req.params;

    if (nuevaContrasena !== nuevaContrasena2) {
      throw new Error("La nueva contraseña no coincide");
    }

    let usuarioExistente;
    try {
      [usuarioExistente] = await connection.query(
        `
            SELECT *
            FROM usuarios
            WHERE id=?
            `,
        [id]
      );
    } catch (error) {
      throw new Error("No se pudo buscar al usuario en la base de datos");
    }

    const contrasenaCodificada = usuarioExistente[0].contraseña;

    try {
      const isValid = await bcrypt.compare(contrasenaVieja, contrasenaCodificada);
      if (isValid === false) {
        throw new Error("La contraseña introducida no coincide con la depositada en base de datos");
      }
    } catch (error) {
      throw new Error("no se han podido comparar");
    }

    let contrasenaBd;
    try {
      contrasenaBd = await bcrypt.hash(nuevaContrasena, 10);
    } catch (error) {
      throw new Error("la contraseña no se pudo codificar");
    }

    try {
      await connection.query(
        `
            UPDATE usuarios
            SET contraseña=?
            WHERE id=?
            `,
        [contrasenaBd, id]
      );
    } catch (error) {
      throw new Error("el usuario no se pudo actualizar");
    }

    res.send("contraseña actualizada");
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { editarContrasena };
