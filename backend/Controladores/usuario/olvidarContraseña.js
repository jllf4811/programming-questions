const { getConnection } = require("../../db");
const bcrypt = require("bcrypt");
const generator = require("generate-password");
const { enviarEmail } = require("../../helpers");

async function olvidarContrasena(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { email } = req.body;

    if (!email) {
      throw new Error("No has introducido ningun correo electronico");
    }
    let usuarioExistente;
    try {
      [usuarioExistente] = await connection.query(
        `
        SELECT *
        FROM usuarios
        WHERE email=?
        `,
        [email]
      );
    } catch (error) {
      throw new Error("No se pudo buscar usuario en la base de datos");
    }

    if (usuarioExistente.length < 1) {
      throw new Error(
        "No existe usuario en la base de datos con ese código para resetear su contraseña"
      );
    }

    const [contrasena] = generator.generateMultiple(1, {
      length: 10,
      uppercase: false,
    });

    let contrasenaDb;
    try {
      contrasenaDb = await bcrypt.hash(contrasena, 10);
    } catch (error) {
      throw new Error("la contraseña no se pudo codificar");
    }

    try {
      await connection.query(
        `
            UPDATE usuarios
            SET contraseña=?
            WHERE id=?
            `,
        [contrasenaDb, usuarioExistente[0].id]
      );

      await enviarEmail({
        to: email,
        subject: "Cambiaste la contraseña programmingQuestions",
        text: `Se ha generado una contraseña se te recomienda editarla cuando inicies sesión.
        ${contrasena}`,
      });
    } catch (error) {
      throw new Error("No se pudo actualizar la contraseña");
    }

    res.send("llega al final");
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { olvidarContrasena };
