const { getConnection } = require("../../db");

async function activarUsuario(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { codigoRegistro } = req.params;

    let usuarios;
    try {
      [usuarios] = await connection.query(
        `
      SELECT codigoRegistro
      FROM usuarios
      WHERE codigoRegistro=?
      `,
        [codigoRegistro]
      );
    } catch (error) {
      throw new Error("no se encontro usuario con ese codigo");
    }
    if (usuarios.length < 1) {
      throw new Error("no hay usuario con ese codigo de validacion");
    }

    try {
      await connection.query(
        `
      UPDATE usuarios
      SET activar=true, codigoRegistro = NULL
      WHERE codigoRegistro=?
      `,
        [codigoRegistro]
      );
    } catch (error) {
      throw new Error("No se pudo activar usuario");
    }

    res.send({
      status: "ok",
      message: "El usuario ha sido activado",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { activarUsuario };
