const { getConnection } = require("../../db");
const bcrypt = require("bcrypt");

const jsonwebtoken = require("jsonwebtoken");

async function iniciarSesion(req, res, next) {
  let connection;
  try {
    connection = await getConnection();

    const { email, contrasena } = req.body;

    if (!email || !contrasena) {
      throw new Error("Faltan datos");
    }

    let usuarios;
    try {
      [usuarios] = await connection.query(
        `
      SELECT id, email, contraseña, activar, rol, nombre
      FROM usuarios
      WHERE email=? 
      `,
        [email]
      );
    } catch (error) {
      throw new Error("no se ha podido encontrar email");
    }

    if (usuarios.length < 1) {
      throw new Error("no se ha encontrado usuario");
    }

    if (usuarios[0].activar !== 1) {
      throw new Error("El usuario no esta activo");
    }

    const hash = usuarios[0].contraseña;

    const contrasenaValida = bcrypt.compareSync(contrasena, hash);

    if (!contrasenaValida) {
      throw new Error("la contraseña no coincide");
    }

    const tokenInfo = {
      id: usuarios[0].id,
      rol: usuarios[0].rol,
    };
    console.log(tokenInfo);

    const token = jsonwebtoken.sign(tokenInfo, process.env.SECRET, {
      expiresIn: "30d",
    });
    console.log(token);
    res.send({
      status: "ok",
      message: "Se ha iniciado sesión",
      token: token,
      id: usuarios[0].id,
      rol: usuarios[0].rol,
      nombre: usuarios[0].nombre,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = { iniciarSesion };
