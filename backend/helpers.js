const path = require("path");
const fs = require("fs").promises;
const sharp = require("sharp");
const uuid = require("uuid");
const sendgrid = require("@sendgrid/mail");
sendgrid.setApiKey(process.env.APIKEY);

const directorioSubida = path.join(__dirname, process.env.DIR_UPLOAD);

async function enviarEmail({ to, subject, text }) {
  try {
    const msg = {
      to: to,
      from: process.env.SEND_FROM,
      subject: subject,
      text: text,
    };
    await sendgrid.send(msg);
  } catch (error) {
    console.error(error);

    throw new Error("error enviando email");
  }
}

async function subidasImagenes({ archivo, directorio }) {
  const directorioImagen = path.join(directorioSubida, directorio);

  await fs.mkdir(directorioImagen, { recursive: true });

  const imagen = sharp(archivo.data);

  const nombreArchivo = `${uuid.v4()}.jpg`;

  await imagen.toFile(path.join(directorioImagen, nombreArchivo));

  return nombreArchivo;
}

module.exports = { subidasImagenes, enviarEmail };
