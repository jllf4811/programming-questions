const { getConnection } = require("./db");
const bcrypt = require("bcrypt");
const faker = require("faker");
async function main() {
  let connection;
  try {
    connection = await getConnection();
    await connection.query(`SET GLOBAL FOREIGN_KEY_CHECKS=0;`);
    await connection.query(`DROP TABLE IF EXISTS usuarios_tecnologias`);
    await connection.query(`DROP TABLE IF EXISTS valoracion`);
    await connection.query(`DROP TABLE IF EXISTS categorias`);
    await connection.query(`DROP TABLE IF EXISTS tecnologias`);
    await connection.query(`DROP TABLE IF EXISTS preguntas_tecnologias`);
    await connection.query(`DROP TABLE IF EXISTS preguntas`);
    await connection.query(`DROP TABLE IF EXISTS usuarios`);
    await connection.query(`DROP TABLE IF EXISTS respuestas`);

    await connection.query(`
        CREATE TABLE usuarios(
        id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        nombre VARCHAR(50) NOT NULL,
        email VARCHAR(50) NOT NULL UNIQUE,
        contraseña TINYTEXT NOT NULL,
        activar BOOLEAN DEFAULT FALSE,
        codigoRegistro TINYTEXT,
        rol ENUM("estudiante", "experto", "administrador") DEFAULT "estudiante" NOT NULL,
        avatar TINYTEXT, 
        fechaRegistro DATETIME NOT NULL,
        fechaActualizacion DATETIME NOT NULL
        );
        `);

    console.log("Tabla usuarios creada");

    await connection.query(`
        CREATE TABLE preguntas(
        id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        titulo VARCHAR(200) NOT NULL,
        FULLTEXT (titulo),
        textoPregunta TEXT NOT NULL,
        imagen TINYTEXT,
        fechaPublicacionPregunta DATETIME NOT NULL,
        fechaActualizacionPregunta DATETIME NOT NULL,
        id_usuario INT UNSIGNED,
        FOREIGN KEY (id_usuario) REFERENCES usuarios(id) ON DELETE CASCADE
        ) ENGINE=MyISAM;
    `);

    console.log("Tabla preguntas creada");

    await connection.query(`
        CREATE TABLE respuestas(
        id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        textoRespuesta TEXT NOT NULL,
        fechaPublicacionRespuestas DATETIME NOT NULL,
        fechaActualizacionRespuestas DATETIME NOT NULL,
        id_usuario INT UNSIGNED,
        FOREIGN KEY (id_usuario) REFERENCES usuarios(id) ON DELETE CASCADE,
        id_pregunta INT UNSIGNED,
        FOREIGN KEY (id_pregunta) REFERENCES preguntas(id) ON DELETE CASCADE 
        );
    `);

    await connection.query(`
        CREATE TABLE valoracion(
        id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        voto ENUM ("1", "-1") DEFAULT NULL,
        id_respuesta INT UNSIGNED,
        FOREIGN KEY (id_respuesta) REFERENCES respuestas(id) ON DELETE CASCADE,
        id_usuario INT UNSIGNED,
        FOREIGN KEY (id_usuario) REFERENCES usuarios(id) ON DELETE CASCADE
        );  
        `);

    console.log("Tabla valoración creada");

    await connection.query(`
    Create TABLE usuarios_tecnologias(
    id_usuario INT UNSIGNED,
    FOREIGN KEY (id_usuario) REFERENCES usuarios(id) ON DELETE CASCADE,
    id_tecnologia INT UNSIGNED,
    FOREIGN KEY (id_tecnologia) REFERENCES tecnologias(id)
    );
    `);

    console.log("Tabla usuarios_tecnologias creada");

    await connection.query(`
      Create TABLE preguntas_tecnologias(
      id_pregunta INT UNSIGNED,
      FOREIGN KEY (id_pregunta) REFERENCES preguntas(id) ON DELETE CASCADE, 
      id_tecnologia INT UNSIGNED,
      FOREIGN KEY (id_tecnologia) REFERENCES tecnologias(id) 
      );
    `);

    await connection.query(`
    CREATE TABLE tecnologias(
      id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
      tecnologiaLenguajes VARCHAR(25) NOT NULL UNIQUE
      );
    `);

    console.log("Tabla tecnologias creada");

    ////////////////////////////////////////Poblador
    const contraseña = await bcrypt.hash("123456", 10);

    await connection.query(
      `
    INSERT INTO usuarios(
      nombre,
      email,
      contraseña,
      rol,
      activar,
      fechaRegistro,
      fechaActualizacion
    )
    VALUES(?,?,?,?,?,?,?)
    `,
      [
        "administrador",
        "administrador@gmail.com",
        contraseña,
        "administrador",
        "1",
        new Date(),
        new Date(),
      ]
    );

    const usuairo = 5;
    for (let i = 0; i < usuairo; i++) {
      const nombre = faker.name.findName();
      const email = faker.internet.email();
      await connection.query(
        `
      INSERT INTO usuarios(
        nombre,
        email,
        contraseña,
        rol,
        activar,
        fechaRegistro,
        fechaActualizacion
      )
      VALUES(?,?,?,?,?,?,?)
      `,
        [nombre, "1" + email, contraseña, "estudiante", "1", new Date(), new Date()]
      );
    }

    console.log("Tabla Preguntas_tecnologias creada");
  } catch (error) {
    console.error(error.message);
  } finally {
    console.log("ok");
    if (connection) connection.release();
    process.exit();
  }
}

main();
